
This Module runs with the Tryton application platform.

This module is developed and tested over a Tryton server and core modules.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-productive_process/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-productive_process)

Installing
----------

See INSTALL

Support
-------

If you encounter any problems with this module, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list,
wiki or IRC channel:

*  http://doc.tryton-erp.es/
*  http://gitlab.com/datalifeit/trytond-productive_process
*  http://groups.tryton.org/
*  http://wiki.tryton.org/
*  irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT


For more information please visit the Datalife web site:

  http://www.datalifeit.es/
