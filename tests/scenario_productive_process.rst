===========================
Productive Process Scenario
===========================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> import datetime
	>>> today = datetime.date.today()


Install agro_execution_productive_process::

    >>> config = activate_modules('productive_process')


Create productive process::

    >>> Process = Model.get('productive.process')
    >>> process1 = Process(name='Process 1')
    >>> process1.save()
