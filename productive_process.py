# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool
from trytond.model import ModelSQL, ModelView
from trytond.pyson import Equal, Eval
from trytond.transaction import Transaction

__all__ = ['ProductiveProcess', 'ProductiveProcDocMixin', 'PYSON_NULL_PROCESS']


PYSON_NULL_PROCESS = Equal(Eval('productive_process', None), None)


class ProductiveProcess(ModelSQL, ModelView):
    """Productive process"""
    __name__ = 'productive.process'

    name = fields.Char('Name', translate=True, select=True)


class ProductiveProcDocMixin(object):

    productive_process = fields.Many2One('productive.process',
        'Productive process', ondelete='RESTRICT', required=True, select=True)

    @classmethod
    def __setup__(cls):
        super(ProductiveProcDocMixin, cls).__setup__()
        if getattr(cls, 'state', None):
            cls.productive_process.states['readonly'] = (
                Eval('state') != 'draft')
            cls.productive_process.depends.append('state')

    @staticmethod
    def default_productive_process():
        transaction = Transaction()
        process = transaction.context.get('productive_process', None)
        if process:
            return process

        pool = Pool()
        Process = pool.get('productive.process')
        processes = Process.search([])
        if len(processes) == 1:
            return processes[0].id
        return None
