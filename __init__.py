# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from .productive_process import (ProductiveProcess,
    ProductiveProcDocMixin, PYSON_NULL_PROCESS)

__all__ = ['ProductiveProcDocMixin', 'PYSON_NULL_PROCESS']


def register():
    Pool.register(
        ProductiveProcess,
        module='productive_process', type_='model')
